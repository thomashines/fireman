const FUEL_MAX = 8;
const HEAT_MAX = 4;
const WATER_MAX = 32;

const GRASS_FUEL_MAX = 0.25 * FUEL_MAX;
const URBAN_WATER_MAX = 0.25 * FUEL_MAX;

class Firemap {
  constructor(context, input_handler, pos_x, pos_y, width_real, height_real,
              width_cells, height_cells) {
    this.context = context;
    this.input_handler = input_handler;
    this.pos_x = pos_x;
    this.pos_y = pos_y;
    this.width_real = width_real;
    this.height_real = height_real;
    this.width_cells = width_cells;
    this.height_cells = height_cells;

    this.ambient_wind_x = 0;
    this.ambient_wind_y = 0;
    this.cell_click_handlers = [];
    this.cell_height_real = this.height_real / this.height_cells;
    this.cell_width_real = this.width_real / this.width_cells;
    this.cells = [];
    this.cells_swap = [];
    this.current_total_fires = 0;
    this.current_total_heat = 0;
    this.highlighted_cells = {};
    this.seed = 0;
    this.total_fire_transitions = 0;
    this.total_fuel_burned = 0;
    this.winds = [];

    let self = this;
    let cell_pos_x, cell_pos_y, cell_width, cell_height;
    for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
      for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
        let cell = new Cell(cell_x, cell_y);
        this.cells.push(cell);
        this.cells_swap.push(new Cell(cell_x, cell_y));
        [cell_pos_x, cell_pos_y, cell_width, cell_height] =
          this.getCellRect(cell_x, cell_y);
        this.input_handler.registerMouseHandler("click",
          cell_pos_x + 1, cell_pos_y + 1,
          cell_pos_x + cell_width - 1, cell_pos_y + cell_height - 1,
          (cursor_x, cursor_y, mouse_event, details) => {self.cellClicked(cell)});
      }
    }
  }

  applyWinds() {
    for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
      for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
        let cell = this.getCell(cell_x, cell_y);
        cell.wind_x = this.ambient_wind_x;
        cell.wind_y = this.ambient_wind_y;
        for (const [wind_cell_x, wind_cell_y, radius, wind_x, wind_y] of this.winds) {
          const dist_x = wind_cell_x - cell_x;
          const dist_y = wind_cell_y - cell_y;
          const radius_squared = radius * radius;
          const dist_squared = dist_x * dist_x + dist_y * dist_y;
          if (dist_squared <= radius_squared) {
            const dist = Math.sqrt(dist_squared);
            const ratio = Math.max(0, (radius - dist) / radius);
            cell.wind_x += Math.floor(ratio * wind_x);
            cell.wind_y += Math.floor(ratio * wind_y);
          }
        }
      }
    }
  }

  cellClicked(cell) {
    this.cell_click_handlers.forEach((callback) => {callback(cell);});
  }

  clearWindPreview() {
    this.cells.forEach((cell) => {
      cell.wind_x_preview = 0;
      cell.wind_y_preview = 0;
    });
  }

  generateMap(seed = null) {
    if (seed != null)
    {
      this.setSeed(seed);
    }

    // Choose a scenario style
    const scenario_style = this.randomInt(1, 1);
    if (scenario_style == 0) {
      // Full rain forest
      this.cells.forEach((cell) => {
        cell.type = FOREST;
        cell.fuel = 3;
        cell.water = 1;
      });
    } else if (scenario_style == 1) {
      // Use perlin noise to set fuel and water
      noise.seed(this.randomInt(1, 65535));
      for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
        for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
          let cell = this.getCell(cell_x, cell_y);
          cell.fuel = Math.floor((noise.simplex2(cell_x / 10, cell_y / 10) + 1) * (FUEL_MAX / 2));
          if ((cell.fuel > FUEL_MAX) || (cell.fuel < 0)) {
            console.log("nah fuel");
          }
        }
      }
      noise.seed(this.randomInt(0, 65535));
      for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
        for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
          let cell = this.getCell(cell_x, cell_y);
          cell.water = Math.floor((noise.simplex2(cell_x / 10, cell_y / 10) + 1) * (WATER_MAX / 2));
          if ((cell.water > WATER_MAX) || (cell.water < 0)) {
            console.log("nah water");
          }
        }
      }

      for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
        for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
          let cell = this.getCell(cell_x, cell_y);
          if ((cell.fuel == 0) && (cell.water == 0)) {
            // Set cells without fuel or water to empty
            cell.type = EMPTY;
          } else if ((cell.fuel == 0) && (cell.water > 0)) {
            // Set cells with water and without fuel to water
            cell.type = WATER;
          } else if ((cell.fuel > 0) && (cell.water <= URBAN_WATER_MAX)) {
            // Set cells with fuel and without water to urban
            cell.water = 0;
            cell.type = URBAN;
          } else if ((cell.fuel > GRASS_FUEL_MAX) && (cell.water > URBAN_WATER_MAX)) {
            // Set cells with max fuel and some water to forest
            cell.type = FOREST;
          } else if ((cell.fuel <= GRASS_FUEL_MAX) && (cell.water > URBAN_WATER_MAX)) {
            // Set cells with 1 fuel and some water to grass
            cell.type = GRASS;
          } else {
            // Set other cells to ashes?
            cell.type = ASHES;
            console.log("why are there ashes?");
          }

          // Clear properties
          cell.heat = 0;
          cell.wind_x = 0;
          cell.wind_x_preview = 0;
          cell.wind_y = 0;
          cell.wind_y_preview = 0;
        }
      }

    } else if (scenario_style == 2) {
      // Start with forest
      // Place some cities around the map
      // Place some lakes around the map
      // Connect them all with roads
      // Surround cities, lakes and roads with grass
      // Randomly scatter higher density forests
    } else {
      console.log("this shouldn't happen");
      this.generateMap();
    }

    // Generate ambient wind
    this.ambient_wind_x = this.randomInt(-4, 4);
    this.ambient_wind_y = this.randomInt(-4, 4);

    // Generate starting rain

    // Apply initial weather
    for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
      for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
        let cell = this.getCell(cell_x, cell_y);
        cell.wind_x = this.ambient_wind_x;
        cell.wind_y = this.ambient_wind_y;
      }
    }

    // Reset statistics
    this.current_total_fires = 0;
    this.current_total_heat = 0;
    this.total_fire_transitions = 0;
    this.total_fuel_burned = 0;
  }

  getFlamableCells() {
    let flamable_cells = 0;
    this.cells.forEach((cell) => {
      if (cell.fuel > 0) {
        ++flamable_cells;
      }
    });
    return flamable_cells;
  }

  getCell(cell_x, cell_y, from_swap = false) {
    if ((cell_x < 0) || (cell_x >= this.width_cells) ||
        (cell_y < 0) || (cell_y >= this.height_cells)) {
      return null;
    }
    const cell_index = this.getCellIndex(cell_x, cell_y);
    if (from_swap) {
      return this.cells_swap[cell_index];
    }
    return this.cells[cell_index];
  }

  getCellAtReal(real_x, real_y, from_swap = false) {
    return this.getCell(
      Math.floor((real_x - this.pos_x) / this.cell_width_real),
      Math.floor((real_y - this.pos_y) / this.cell_height_real),
      from_swap);
  }

  getCellIndex(cell_x, cell_y) {
    return cell_y * this.width_cells + cell_x;
  }

  getCellRect(cell_x, cell_y) {
    const pos_x = this.pos_x + cell_x * this.cell_width_real;
    const pos_y = this.pos_y + cell_y * this.cell_height_real;
    const width = this.cell_width_real;
    const height = this.cell_height_real;
    return [pos_x, pos_y, width, height];
  }

  getCircleNeighbours(cell_x, cell_y, radius, from_swap = false) {
    let neighbours = [];
    const radius_squared = radius * radius;
    for (let neighbour_y = cell_y - radius; neighbour_y <= cell_y + radius; ++neighbour_y) {
      const dist_y = neighbour_y - cell_y;
      for (let neighbour_x = cell_x - radius; neighbour_x <= cell_x + radius; ++neighbour_x) {
        const dist_x = neighbour_x - cell_x;
        const dist_squared = dist_x * dist_x + dist_y * dist_y;
        if (dist_squared <= radius_squared) {
          let neighbour = this.getCell(neighbour_x, neighbour_y, from_swap);
          if (neighbour != null) {
            neighbours.push(neighbour);
          }
        }
      }
    }
    return neighbours;
  }

  getCurrentTotalFires() {
    return this.current_total_fires;
  }

  getCurrentTotalHeat() {
    return this.current_total_heat;
  }

  getDownwindNeighbours(cell_x, cell_y, wind_x, wind_y, from_swap = false) {
    let neighbours = [];
    const wind_dir_x = Math.sign(wind_x) || 1;
    const wind_dir_y = Math.sign(wind_y) || 1;
    for (let dy = 0; dy != wind_y + wind_dir_y; dy += wind_dir_y) {
      for (let dx = 0; dx != wind_x + wind_dir_x; dx += wind_dir_x) {
        let neighbour = this.getCell(cell_x + dx, cell_y + dy, from_swap);
        if (neighbour != null) {
          neighbours.push(neighbour);
        }
      }
    }
    return neighbours;
  }

  getFuelAvailable() {
    let fuel_available = 0;
    this.cells.forEach((cell) => {fuel_available += cell.fuel;});
    return fuel_available;
  }

  getNeighbours(cell_x, cell_y, from_swap = false) {
    let neighbours = [];
    for (let dy = -1; dy <= 1; ++dy) {
      for (let dx = -1; dx <= 1; ++dx) {
        let neighbour = this.getCell(cell_x + dx, cell_y + dy, from_swap);
        if (neighbour != null) {
          neighbours.push(neighbour);
        }
      }
    }
    return neighbours;
  }

  getTotalFireTransitions() {
    return this.total_fire_transitions;
  }

  getTotalFuelBurned() {
    return this.total_fuel_burned;
  }

  highlightCell(cell, highlight = true) {
    let changed = false;
    if (cell == null) {
      if (highlight) {
        this.cells.forEach((cell) => {
          if (this.highlightCell(cell, true)) {
            changed = true;
          }
        });
      } else {
        Object.entries(this.highlighted_cells).forEach(
          ([cell_index, is_highlighted]) => {
            if (is_highlighted) {
              this.cells[cell_index].is_highlighted = false;
              changed = true;
            }
          });
        this.highlighted_cells = {};
      }
    } else {
      const cell_index = this.getCellIndex(cell.cell_x, cell.cell_y);
      if (!this.highlighted_cells[cell_index] == highlight) {
        cell.is_highlighted = highlight;
        this.highlighted_cells[cell_index] = highlight;
        changed = true;
      }
    }
    return changed;
  }

  pushWind(cell_x, cell_y, radius, wind_x, wind_y) {
    this.winds.push([cell_x, cell_y, radius, wind_x, wind_y]);
    this.applyWinds();
  }

  randomInt(min, max) {
    return min + Math.floor(
      (Math.sin(this.seed++ * this.seed++) + 1.0) * 0.5 * (max - min + 1));
  }

  registerCellClickHandler(callback) {
    this.cell_click_handlers.push(callback);
  }

  render() {
    this.renderCells();
    this.renderWind();
  }

  renderCells() {
    let self = this;
    this.cells.forEach((cell) => {self.renderCell(cell);});
  }

  renderCell(cell) {
    let pos_x, pos_y, width, height;
    [pos_x, pos_y, width, height] = this.getCellRect(cell.cell_x, cell.cell_y);

    // Draw the base image
    const image_width = CELL_TYPES_IMAGES[cell.type].width;
    const image_height = CELL_TYPES_IMAGES[cell.type].height;
    this.context.globalAlpha = 1.0;
    this.context.drawImage(CELL_TYPES_IMAGES[cell.type], 0, 0,
      image_width, image_height, pos_x, pos_y, width, height);

    // Draw overlays
    // const previous_global_alpha = this.context.globalAlpha;
    if (cell.heat > 0) {
      this.context.globalAlpha = (cell.heat / HEAT_MAX) * 0.5;
      this.context.fillStyle = "#F00";
      this.context.fillRect(pos_x, pos_y, width, height);
    }
    if ((cell.type != WATER) && (cell.water > 0)) {
      this.context.globalAlpha = (cell.water / WATER_MAX) * 0.2;
      this.context.fillStyle = "#008";
      this.context.fillRect(pos_x, pos_y, width, height);
    }
    if (cell.is_highlighted) {
      this.context.globalAlpha = 0.2;
      this.context.fillStyle = "#FFF";
      this.context.fillRect(pos_x, pos_y, width, height);
    }
    // this.context.globalAlpha = previous_global_alpha;
  }

  renderWind() {
    // For each cell, if the wind vector can and should be rendered, render it
    // const previous_global_alpha = this.context.globalAlpha;
    this.context.strokeStyle = "#FFF";
    this.context.lineWidth = 4;
    this.context.globalAlpha = 0.5;
    let self = this;
    this.cells.forEach((cell) => {
      if ((cell.is_highlighted) || (cell.wind_x_preview != 0) || (cell.wind_y_preview != 0)) {
        const wind_x = cell.wind_x + cell.wind_x_preview;
        const wind_y = cell.wind_y + cell.wind_y_preview;
        if ((wind_x != 0) || (wind_y != 0)) {
          const downwind_cell = self.getCell(cell.cell_x + wind_x,
                                             cell.cell_y + wind_y);
          if (downwind_cell != null) {
            const line_x1 = self.pos_x + (cell.cell_x + 0.5) * self.cell_width_real;
            const line_y1 = self.pos_y + (cell.cell_y + 0.5) * self.cell_height_real;
            const line_x2 = self.pos_x + (downwind_cell.cell_x + 0.5) * self.cell_width_real;
            const line_y2 = self.pos_y + (downwind_cell.cell_y + 0.5) * self.cell_height_real;
            self.context.beginPath();
            self.context.moveTo(line_x1, line_y1);
            self.context.lineTo(line_x2, line_y2);
            self.context.stroke();
          }
        }
      }
    });
    // For each wind, render it
    this.context.strokeStyle = "#00F";
    this.context.lineWidth = 4;
    this.context.globalAlpha = 0.5;
    for (const [wind_cell_x, wind_cell_y, radius, wind_x, wind_y] of this.winds) {
      const line_x1 = this.pos_x + (wind_cell_x + 0.5) * this.cell_width_real;
      const line_y1 = this.pos_y + (wind_cell_y + 0.5) * this.cell_height_real;
      const line_x2 = this.pos_x + (wind_cell_x + wind_x + 0.5) * this.cell_width_real;
      const line_y2 = this.pos_y + (wind_cell_y + wind_y + 0.5) * this.cell_height_real;
      this.context.beginPath();
      this.context.moveTo(line_x1, line_y1);
      this.context.lineTo(line_x2, line_y2);
      this.context.stroke();
      this.context.beginPath();
      this.context.arc(line_x1, line_y1, radius * this.cell_width_real, 0, 2 * Math.PI);
      this.context.stroke();
    }
    // this.context.globalAlpha = previous_global_alpha;
  }

  setCellType(cell, type) {
    if (type != cell.type) {
      cell.type = type;
      if (cell.type == FIRE) {
        this.total_fire_transitions += 1;
      }
    }
  }

  setSeed(seed) {
    this.seed = seed;
  }

  shiftWind() {
    this.winds.shift();
    this.applyWinds();
  }

  tickCells() {
    // TICK RULES
    // Properties
    //   * fuel
    //     * all forests with water and light gain 1 fuel
    //     * all fires with fuel lose 1 fuel
    //   * heat
    //     * all fires add 1 heat to every adjacent cell
    //     * all fires add 1 heat to every cell within wind range
    //     * all cells lose 1 heat
    //   * water
    //     * all non-water cells in rain gain 1 water
    //     * all urbans and roads lose 1 water
    //     * all non-water cells lose 1 water per heat
    // Type
    //   * any fire with 1 fuel becomes ashes
    //   * any cell with fuel and without water that is adjacent to a fire
    //     becomes a fire
    // Weather
    //   * rain moves with wind

    // Copy cells into swap
    for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
      for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
        const cell_current = this.getCell(cell_x, cell_y, false);
        let cell_swap = this.getCell(cell_x, cell_y, true);
        cell_swap.copy(cell_current);
      }
    }

    // Update cells according to rules (using swap as source)
    for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
      for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
        const cell_old = this.getCell(cell_x, cell_y, true);
        let cell_new = this.getCell(cell_x, cell_y, false);

        // Get the new neighbours
        const neighbours_old = this.getNeighbours(cell_x, cell_y, true);
        let neighbours_new = this.getNeighbours(cell_x, cell_y, false);
        let downwind_neighbours_new = this.getDownwindNeighbours(
          cell_x, cell_y, cell_old.wind_x, cell_old.wind_y, false);

        // Add fuel
        if ((cell_old.type == FOREST) && (cell_old.water > 0) &&
            (cell_old.light > 0)) {
          cell_new.fuel += 1;
        }
        // Remove fuel
        if (cell_old.type == FIRE) {
          cell_new.fuel -= 1;
        }

        // Add heat
        if (cell_old.type == FIRE) {
          neighbours_new.forEach((neighbour_new) => {
            neighbour_new.heat += 1;
          });
          downwind_neighbours_new.forEach((downwind_neighbour_new) => {
            downwind_neighbour_new.heat += 1;
          });
        }
        // Remove heat
        cell_new.heat -= 1;

        // Add water
        if ((cell_old.type != WATER) && (cell_old.rain > 0)) {
          cell_new.water += 1;
        }
        // Remove water
        if (cell_old.type != WATER) {
          cell_new.water -= cell_old.heat;
        }

        // Transition type
        if ((cell_old.type == FIRE) && (cell_old.fuel <= 1)) {
          cell_new.type = ASHES;
        }
        const is_adjacent_to_fire = neighbours_old.some((neighbour_old) => {
          return neighbour_old.type == FIRE;
        });
        if ((cell_old.type != FIRE) && (cell_old.fuel > 0) &&
            (cell_old.water == 0) && is_adjacent_to_fire) {
          cell_new.type = FIRE;
        }

        // Update weather
      }
    }

    // Clamp and track properties
    this.current_total_fires = 0;
    this.current_total_heat = 0;
    for (let cell_y = 0; cell_y < this.height_cells; ++cell_y) {
      for (let cell_x = 0; cell_x < this.width_cells; ++cell_x) {
        const cell_old = this.getCell(cell_x, cell_y, true);
        let cell_new = this.getCell(cell_x, cell_y, false);

        // Clamp fuel
        cell_new.fuel = Math.max(0, Math.min(cell_new.fuel, FUEL_MAX));
        // Track fuel burned
        if ((cell_old.type == FIRE) && (cell_new.fuel < cell_old.fuel)) {
          this.total_fuel_burned += (cell_old.fuel - cell_new.fuel);
        }

        // Clamp heat
        cell_new.heat = Math.max(0, Math.min(cell_new.heat, HEAT_MAX));
        // Track heat
        this.current_total_heat += cell_new.heat;

        // Clamp water
        cell_new.water = Math.max(0, Math.min(cell_new.water, WATER_MAX));

        // Track fires
        if (cell_new.type == FIRE) {
          this.current_total_fires += 1;
          if (cell_old.type != FIRE) {
            this.total_fire_transitions += 1;
          }
        }
      }
    }
  }

  windPreview(cell_x, cell_y, radius, wind_x, wind_y) {
    this.clearWindPreview();
    const radius_squared = radius * radius;
    this.getCircleNeighbours(cell_x, cell_y, radius).forEach((cell) => {
      const dist_x = cell.cell_x - cell_x;
      const dist_y = cell.cell_y - cell_y;
      // const dist_squared = dist_x * dist_x + dist_y * dist_y;
      // const ratio = Math.max(0, (radius_squared - dist_squared) / radius_squared);
      const dist = Math.sqrt(dist_x * dist_x + dist_y * dist_y);
      const ratio = Math.max(0, (radius - dist) / radius);
      cell.wind_x_preview = Math.floor(ratio * wind_x);
      cell.wind_y_preview = Math.floor(ratio * wind_y);
    });
  }
}
