const CELL_TYPES_ARRAY = [
  ["ASHES",  "Ashes", "#000000", "img/ashes.png"],
  ["EMPTY",  "Empty", "#482303", "img/empty.png"],
  ["FIRE",   "Fire", "#c53f0b", "img/fire.png"],
  ["FOREST", "Forest", "#135a00", "img/forest.png"],
  ["GRASS",  "Grass", "#759b0a", "img/grass.png"],
  ["ROAD",   "Road", "#2a2a2a", "img/road.png"],
  ["URBAN",  "Urban", "#2d261e", "img/urban.png"],
  ["WATER",  "Water", "#204a69", "img/water.png"],
]
let CELL_TYPES_NAMES = {};
let CELL_TYPES_WORDS = {};
let CELL_TYPES_STYLES = {};
let CELL_TYPES_IMAGES = {};
CELL_TYPES_ARRAY.forEach((cell_type, cell_type_id) => {
  const cell_type_name = cell_type[0];
  const cell_type_word = cell_type[1];
  const cell_type_style = cell_type[2];
  const cell_type_image = cell_type[3];
  window[cell_type_name] = cell_type_id;
  CELL_TYPES_NAMES[cell_type_id] = cell_type_name;
  CELL_TYPES_WORDS[cell_type_id] = cell_type_word;
  CELL_TYPES_STYLES[cell_type_id] = cell_type_style;
  CELL_TYPES_IMAGES[cell_type_id] = new Image();
  CELL_TYPES_IMAGES[cell_type_id].src = cell_type_image;
});

class Cell {
  constructor(cell_x, cell_y) {
    this.cell_x = cell_x;
    this.cell_y = cell_y;

    // Type
    this.type = EMPTY;

    // Properties
    this.fuel = 0;
    this.heat = 0;
    this.water = 0;

    // Weather
    this.light = 0;
    this.rain = 0;
    this.total_wind_x = 0;
    // this.total_wind_x_preview = 0;
    this.total_wind_y = 0;
    // this.total_wind_y_preview = 0;
    this.wind_x = 0;
    this.wind_x_preview = 0;
    this.wind_y = 0;
    this.wind_y_preview = 0;
  }

  copy(other_cell) {
    this.cell_x = other_cell.cell_x;
    this.cell_y = other_cell.cell_y;
    this.fuel = other_cell.fuel;
    this.heat = other_cell.heat;
    this.light = other_cell.light;
    this.rain = other_cell.rain;
    this.total_wind_x = other_cell.total_wind_x;
    // this.total_wind_x_preview = other_cell.total_wind_x_preview;
    this.total_wind_y = other_cell.total_wind_y;
    // this.total_wind_y_preview = other_cell.total_wind_y_preview;
    this.type = other_cell.type;
    this.water = other_cell.water;
    this.wind_x = other_cell.wind_x;
    this.wind_x_preview = other_cell.wind_x_preview;
    this.wind_y = other_cell.wind_y;
    this.wind_y_preview = other_cell.wind_y_preview;
  }

  toString() {
    let as_string = CELL_TYPES_WORDS[this.type];
    let details = [];
    if (this.fuel > 0) {
      details.push(this.fuel + " fuel");
    }
    if (this.heat > 0) {
      details.push(this.heat + " heat");
    }
    if ((this.type != WATER) && (this.water > 0)) {
      details.push(this.water + " water");
    }
    if (details.length > 0) {
      as_string += " with " + details.join(" and ");
    }
    return as_string;
  }
}
