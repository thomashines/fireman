class Label {
  constructor(context, pos_x, pos_y, font, text = null) {
    this.context = context;
    this.pos_x = pos_x;
    this.pos_y = pos_y;
    this.font = font;
    this.text = text;
    this.render();
  }

  setText(text) {
    this.text = text;
  }

  render() {
    if (this.text != null)
    {
      this.context.font = this.font;
      this.context.fillStyle = "#FFF";
      this.context.fillText(this.text, this.pos_x, this.pos_y);
    }
  }
}
