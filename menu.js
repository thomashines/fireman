const BUTTONS_ARRAY = [
  ["CONTINUE", "Continue"],
  ["HELP", "Show help"],
  ["RESTART", "New map"],
  ["RETRY", "Retry map"],
]
let BUTTONS_IDS = [];
let BUTTONS_NAMES = {};
let BUTTONS_LABELS = {};
BUTTONS_ARRAY.forEach((button, button_id) => {
  const button_name = button[0];
  const button_label = button[1];
  window[button_name] = button_id;
  BUTTONS_IDS.push(button_id);
  BUTTONS_NAMES[button_id] = button_name;
  BUTTONS_LABELS[button_id] = button_label;
});

const HELP_PADDING = 40;
const HELP_TEXT = [
  "Fireman - accurate fire management simulator",
  "",
  "See the Goal label in the bottom bar for your goal,",
  "here are the types:",
  "",
  "Time: keep the fire alive for at least this number of",
  "  ticks.",
  "Energy: burn at least this much fuel.",
  "Destruction: convert at least this many cells to fire.",
  "Heat: I don't know how to explain this, burn things.",
  "",
  "Other concepts:",
  "",
  "Fuel: a fire uses 1 fuel per tick and goes out when it",
  "  reaches 0.",
  "Water: a cell will not burn while it has water.",
  "Heat: heat is blown by wind and reduces water in cells.",
  "Wind: wind blows heat, tends to encourage fire, can",
  "  also slow it.",
];
const HELP_FONT = "12px serif";
const HELP_LINE_HEIGHT = 16;

class Menu {
  constructor(context, input_handler, pos_x, pos_y, width_real, height_real) {
    this.context = context;
    this.input_handler = input_handler;
    this.pos_x = pos_x;
    this.pos_y = pos_y;
    this.width_real = width_real;
    this.height_real = height_real;

    this.button_height = 40;
    this.font = "8px serif";
    this.label_offset_x = 8;
    this.label_offset_y = 24;
    this.lost = false;
    this.show_help = false;
    this.started = false;
    this.won = false;

    this.buttons = {};
    let self = this;
    for (let button_index = 0; button_index < BUTTONS_IDS.length; ++button_index) {
      const button_id = BUTTONS_IDS[button_index];
      this.buttons[button_id] = {
        index: button_index,
        label: new Label(this.context,
          this.pos_x + this.label_offset_x,
          this.pos_y + this.label_offset_y + button_index * this.button_height,
          this.font, BUTTONS_LABELS[button_id]),
        box: [
          this.pos_x + 1,
          this.pos_y + button_index * this.button_height + 1,
          this.width_real - 2,
          this.button_height - 2,
        ],
        is_enabled: false,
        handlers: [],
      };
      const [button_x, button_y, button_width, button_height] = this.buttons[button_id].box;
      this.input_handler.registerMouseHandler("click",
        button_x, button_y, button_x + button_width, button_y + button_height,
        (cursor_x, cursor_y, mouse_event, details) => {
          self.buttonClicked(button_id);
        });
    }

    this.buttons[HELP].is_enabled = true;
    this.buttons[RESTART].is_enabled = true;
  }

  buttonClicked(button_id) {
    if (this.buttons[button_id].is_enabled) {
      this.buttons[button_id].handlers.forEach((callback) => {callback();});
    }
    if (button_id == HELP) {
      if (this.show_help) {
        this.show_help = false;
        this.buttons[HELP].label.setText("Show help");
      } else {
        this.show_help = true;
        this.buttons[HELP].label.setText("Hide help");
      }
    }
  }

  registerButtonClickedHandler(button_id, callback) {
    this.buttons[button_id].handlers.push(callback);
  }

  render() {
    for (const button_id in this.buttons) {
      const button = this.buttons[button_id];
      if (button.is_enabled) {
        const [button_x, button_y, button_width, button_height] = button.box;
        this.context.fillStyle = "#666";
        this.context.fillRect(button_x, button_y, button_width, button_height);
        // this.context.fillStyle = "#FFF";
        button.label.render();
      }
    }

    if (this.show_help) {
      const help_padding = 40;
      this.context.fillStyle = "#444";
      this.context.fillRect(help_padding, help_padding,
        this.pos_x - 2 * help_padding,
        this.pos_y + this.height_real - 2 * help_padding);
      let line_x = help_padding + this.label_offset_x;
      let line_y = help_padding + this.label_offset_y;
      this.context.font = HELP_FONT;
      this.context.fillStyle = "#FFF";
      for (const line of HELP_TEXT) {
        this.context.fillText(line, line_x, line_y);
        line_y += HELP_LINE_HEIGHT;
      }
    }
  }

  setStarted(started) {
    this.started = started;
    if (started) {
      this.won = false;
      this.lost = false;
    }

    this.buttons[RETRY].is_enabled = this.lost || this.started || this.won;
  }

  setWon(won) {
    this.won = won;
    this.lost = !won;

    this.buttons[CONTINUE].is_enabled = this.won;
    this.buttons[RETRY].is_enabled = this.lost || this.started || this.won;
  }
}
