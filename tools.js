const TOOLS_ARRAY = [
  ["BURN",  "#ff0000", "img/fire.png"],
  ["WIND",  "#0000ff", "img/wind.png"],
  // ["RAIN", "#000088", "img/rain.png"],
  // ["SUN",   "#ff00ff", "img/sun.png"],
]
let TOOLS_IDS = [];
let TOOLS_NAMES = {};
let TOOLS_STYLES = {};
let TOOLS_IMAGES = {};
TOOLS_ARRAY.forEach((tool, tool_id) => {
  const tool_name = tool[0];
  const tool_style = tool[1];
  const tool_image = tool[2];
  window[tool_name] = tool_id;
  TOOLS_IDS.push(tool_id);
  TOOLS_NAMES[tool_id] = tool_name;
  TOOLS_STYLES[tool_id] = tool_style;
  TOOLS_IMAGES[tool_id] = new Image();
  TOOLS_IMAGES[tool_id].src = tool_image;
});

class Tools {
  constructor(context, input_handler, pos_x, pos_y, width_real, height_real) {
    this.context = context;
    this.input_handler = input_handler;
    this.pos_x = pos_x;
    this.pos_y = pos_y;
    this.width_real = width_real;
    this.height_real = height_real;

    this.active_tool = BURN;
    this.count_label_offset_x = 8;
    this.count_label_offset_y = 24;
    this.count_labels = {};
    this.counts = {};
    this.font = "16px serif";
    this.highlighted_tool = null;
    this.tool_height = this.width_real;
    this.tool_width = this.width_real;

    let self = this;
    for (let tool_index = 0; tool_index < TOOLS_IDS.length; ++tool_index) {
      const tool_id = TOOLS_IDS[tool_index];
      const tool_x1 = this.pos_x + 1;
      const tool_y1 = this.pos_y + tool_index * this.width_real + 1;
      const tool_x2 = tool_x1 + this.tool_width - 2;
      const tool_y2 = tool_y1 + this.tool_height - 2;
      this.input_handler.registerMouseHandler("click",
        tool_x1, tool_y1, tool_x2, tool_y2,
        (cursor_x, cursor_y, mouse_event, details) => {self.toolClicked(tool_id)});
      this.counts[tool_id] = 0;
      this.count_labels[tool_id] = new Label(
        this.context, tool_x1 + this.count_label_offset_x,
        tool_y1 + this.count_label_offset_y, this.font);
    }
  }

  getActiveTool() {
    return this.active_tool;
  }

  getHighlightedTool() {
    return this.highlighted_tool;
  }

  getToolAtReal(real_x, real_y) {
    if ((real_x < this.pos_x) || (real_x > this.pos_x + this.tool_width) ||
        (real_y < this.pos_y) || (real_y >= this.pos_y + TOOLS_IDS.length * this.tool_height))
    {
      return null;
    }
    return TOOLS_IDS[Math.floor((real_y - this.pos_y) / this.tool_height)];
  }

  getToolCount(tool_id) {
    return this.counts[tool_id];
  }

  highlightTool(tool_id, highlight = true) {
    if ((tool_id == null) || !highlight) {
      if (this.highlighted_tool != null) {
        this.highlighted_tool = null;
        return true;
      }
    } else {
      if (this.highlighted_tool != tool_id) {
        this.highlighted_tool = tool_id;
        return true;
      }
    }
    return false;
  }

  render() {
    this.context.globalAlpha = 1.0;
    for (let tool_index = 0; tool_index < TOOLS_IDS.length; ++tool_index) {
      let tool_id = TOOLS_IDS[tool_index];
      let tool_x = this.pos_x;
      let tool_y = this.pos_y + tool_index * this.tool_height;

      // Draw icon
      this.context.fillStyle = TOOLS_STYLES[tool_id];
      this.context.fillRect(tool_x, tool_y, this.tool_width, this.tool_height);
      let image_width = TOOLS_IMAGES[tool_id].width;
      let image_height = TOOLS_IMAGES[tool_id].height;
      this.context.drawImage(TOOLS_IMAGES[tool_id], 0, 0, image_width,
        image_height, tool_x, tool_y, this.tool_width, this.tool_height);

      // Draw overlays
      // let previous_global_alpha = this.context.globalAlpha;
      if (this.highlighted_tool == tool_id) {
        this.context.globalAlpha = 0.2;
        this.context.fillStyle = "#FFF";
        this.context.fillRect(tool_x, tool_y, this.tool_width, this.tool_height);
      }
      // this.context.globalAlpha = previous_global_alpha;

      this.context.globalAlpha = 1.0;
      if (this.active_tool == tool_id)
      {
        this.context.strokeStyle = "#0F0";
      } else {
        this.context.strokeStyle = "black";
      }
      this.context.beginPath();
      this.context.rect(tool_x + 1, tool_y + 1, this.tool_width - 2, this.tool_height - 2);
      this.context.stroke();
      this.count_labels[tool_id].render();
    }
  }

  toolClicked(tool_id) {
    if (this.active_tool != tool_id)
    {
      this.active_tool = tool_id;
      this.render();
    }
  }

  setToolCount(tool_id, count) {
    this.counts[tool_id] = count;
    this.count_labels[tool_id].setText(count);
  }
}
