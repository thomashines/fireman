# Fireman

A fire management game.

## TODO

* Weather
* Add control for retry or new game
* Add overlay for game over or victory
* Improve map generation
* Add stats screen (victories, total burnt etc.)

## Structure

* Cell map
* Cells rendered based on properties
* New cell properties generated from previous state each tick
* Tools
  * Wind: click and drag to apply wind velocity to cells
  * Rain: click to make a rainy cloud
  * Sunshine: click to intensify sunshine on cell
* Cell properties
  * Class
    * Ash
    * Empty
    * Fire
    * Forest
    * Grass
    * Inhabited
    * Road
  * Fuel
  * Wetness
  * Wind
  * Sun
  * Rain
  * Heat
* Meters
  * Heat: total heat is a measure of how much fire is currently burning
  * Energy: burning fuel gives energy to spend on tools
  * Destruction: burning cells gives destruction
  * Time: number of ticks that fire has been active for
