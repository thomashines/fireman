const GOALS_ARRAY = [
  ["DESTRUCTION", "Destruction", 64, 256],
  ["ENERGY", "Energy", 32, 512],
  ["HEAT", "Heat", 16, 128],
  ["TIME", "Time", 32, 128],
];
let GOALS_IDS = [];
let GOALS_NAMES = {};
let GOALS_WORDS = {};
let GOALS_MINS = {};
let GOALS_MAXS = {};
GOALS_ARRAY.forEach((goal, goal_id) => {
  const goal_name = goal[0];
  const goal_word = goal[1];
  const goal_min = goal[2];
  const goal_max = goal[3];
  window[goal_name] = goal_id;
  GOALS_IDS.push(goal_id);
  GOALS_NAMES[goal_id] = goal_name;
  GOALS_WORDS[goal_id] = goal_word;
  GOALS_MINS[goal_id] = goal_min;
  GOALS_MAXS[goal_id] = goal_max;
});

const BURNS = 1;
const WIND_RADIUS = 8;
const WIND_MAX_X = 8;
const WIND_MAX_Y = 8;
const MAX_WINDS = 2;

class Fireman {
  constructor(canvas, width_real, height_real, grid_size, tick_rate) {
    this.canvas = canvas;
    this.width_real = width_real;
    this.height_real = height_real;
    this.grid_size = grid_size;
    this.tick_rate = tick_rate;

    this.canvas.width = this.width_real;
    this.canvas.height = this.height_real;
    this.context = this.canvas.getContext("2d");

    this.input_handler = new InputHandler(this.canvas);
    // this.input_handler = new InputHandler(this.canvas, this.context);

    this.status_width = this.width_real;
    this.status_height = 80;
    this.status = new Status(this.context, this.input_handler, 0,
                             this.height_real - this.status_height,
                             this.status_width, this.status_height);

    this.tools_width = 80;
    this.tools_height = 2 * this.tools_width;
    this.tools = new Tools(this.context, this.input_handler,
                           this.width_real - this.tools_width, 0,
                           this.tools_width, this.tools_height);

    this.firemap_size = Math.min(this.width_real - this.tools_width,
                                 this.height_real - this.status_height);
    this.firemap_pos_x = (this.width_real - this.tools_width - this.firemap_size) / 2;
    this.firemap_pos_y = (this.height_real - this.status_height - this.firemap_size) / 2;
    this.firemap = new Firemap(this.context, this.input_handler,
                               this.firemap_pos_x, this.firemap_pos_y,
                               this.firemap_size, this.firemap_size,
                               this.grid_size, this.grid_size);

    this.menu_height = 4 * 40;
    this.menu = new Menu(this.context, this.input_handler,
      this.width_real - this.tools_width,
      this.height_real - this.status_height - this.menu_height,
      this.tools_width, this.menu_height);

    this.time = 0;
    this.timer = null;

    this.started = false;
    this.energy = 0;
    this.max_heat = 0;
    this.seed = null;

    this.hovered_cell = null;
    this.hovered_tool = null;

    this.mousedown_cell = null;
    this.mouseup_cell = null;

    this.won = false;
    this.streak = 0;

    let self = this;
    this.firemap.registerCellClickHandler((cell) => {self.cellClicked(cell);});
    this.input_handler.registerMouseHandler("mousemove",
      0, 0, this.width_real, this.height_real,
      (cursor_x, cursor_y, mouse_event, details) => {
        self.mouseMoved(cursor_x, cursor_y);
      });
    this.input_handler.registerMouseHandler("mousedown",
      0, 0, this.width_real, this.height_real,
      (cursor_x, cursor_y, mouse_event, details) => {
        self.mouseDown(cursor_x, cursor_y);
      });
    this.input_handler.registerMouseHandler("mouseup",
      0, 0, this.width_real, this.height_real,
      (cursor_x, cursor_y, mouse_event, details) => {
        self.mouseUp(cursor_x, cursor_y);
      });
    this.menu.registerButtonClickedHandler(CONTINUE, () => {self.continueClicked();});
    this.menu.registerButtonClickedHandler(RESTART, () => {self.restartClicked();});
    this.menu.registerButtonClickedHandler(RETRY, () => {self.retryClicked();});
  }

  cellClicked(cell) {
    let changed = false;
    const tool_id = this.tools.getActiveTool();
    if (tool_id == BURN) {
      const burns_remaining = this.tools.getToolCount(BURN);
      if (burns_remaining <= 0) {
        this.status.log("No burns remain");
      } else if (cell.type == ASHES) {
        this.status.log("Cannot burn ashes");
      } else if (cell.type == FIRE) {
        this.status.log("Already burning");
      } else if (cell.type == WATER) {
        this.status.log("Cannot burn water");
      } else if (cell.fuel <= 0) {
        this.status.log("No fuel to burn");
      } else {
        this.firemap.setCellType(cell, FIRE);
        this.tools.setToolCount(BURN, burns_remaining - 1);
        this.started = true;
        this.menu.setStarted(this.started);
        changed = true;
      }
    }
    this.render();
  }

  continueClicked() {
    this.start(true);
  }

  mouseMoved(cursor_x, cursor_y) {
    let changed = false;

    const hovered_cell = this.firemap.getCellAtReal(cursor_x, cursor_y);
    if (this.hovered_cell != hovered_cell) {
      this.hovered_cell = hovered_cell;

      // Show tool preview
      if (this.hovered_cell != null) {
        if (this.mousedown_cell != null) {
          const tool_id = this.tools.getActiveTool();
          if (tool_id == WIND) {
            this.previewWind(this.mousedown_cell, this.hovered_cell);
          }
        }
      }

      changed = true;
    }

    const hovered_tool = this.tools.getToolAtReal(cursor_x, cursor_y);
    if (this.hovered_tool != hovered_tool) {
      this.hovered_tool = hovered_tool;
      changed = true;
    }

    if (changed) {
      this.render();
    }
  }

  mouseDown(cursor_x, cursor_y) {
    this.mousedown_cell = this.firemap.getCellAtReal(cursor_x, cursor_y);
  }

  mouseUp(cursor_x, cursor_y) {
    let changed = false;

    this.mouseup_cell = this.firemap.getCellAtReal(cursor_x, cursor_y);

    // Clear tool preview
    this.firemap.clearWindPreview();

    // Use tool
    if (this.mousedown_cell != null) {
      const tool_id = this.tools.getActiveTool();
      if (tool_id == WIND) {
        this.useWind(this.mousedown_cell, this.hovered_cell);
        changed = true;
      }
    }

    this.mousedown_cell = null;
    this.mouseup_cell = null;

    if (changed) {
      this.render();
    }
  }

  previewWind(cell_from, cell_to) {
    const wind_x = Math.max(-WIND_MAX_X, Math.min(cell_to.cell_x - cell_from.cell_x, WIND_MAX_X));
    const wind_y = Math.max(-WIND_MAX_Y, Math.min(cell_to.cell_y - cell_from.cell_y, WIND_MAX_Y));
    this.firemap.windPreview(cell_from.cell_x, cell_from.cell_y, WIND_RADIUS,
                             wind_x, wind_y);
  }

  render() {
    this.firemap.highlightCell(null, false);
    if (this.hovered_cell != null) {
      this.firemap.highlightCell(this.hovered_cell);
    }
    if (this.mousedown_cell != null) {
      this.firemap.highlightCell(this.mousedown_cell);
    }
    if (this.mouseup_cell != null) {
      this.firemap.highlightCell(this.mouseup_cell);
    }

    this.tools.highlightTool(null, false);
    if (this.hovered_tool != null) {
      this.tools.highlightTool(this.hovered_tool);
    }

    if (this.hovered_cell != null) {
      this.status.setDetail(this.hovered_cell.toString());
    } else if (this.hovered_tool != null) {
      let detail = "Tool not implemented";
      if (this.hovered_tool == BURN) {
        detail = "Burn: start a fire";
        const count = this.tools.getToolCount(BURN);
        if (count <= 0) {
          detail += " (no uses remain)";
        } else if (count == 1) {
          detail += " (1 use remains)";
        } else if (count <= 0) {
          detail += " (" + count + " uses remain)";
        }
      } else if (this.hovered_tool == WIND) {
        detail = "Wind: click and drag to encourage the fire.";
      }
      this.status.setDetail(detail);
    }

    this.context.clearRect(0, 0, this.width_real, this.height_real);
    this.firemap.render();
    this.tools.render();
    this.status.render();
    this.menu.render();
  }

  restartClicked() {
    this.start(true);
    this.streak = 0;
  }

  retryClicked() {
    this.start(false);
  }

  start(new_seed = true) {
    // Generate map
    if (new_seed || (this.seed == null)) {
      this.seed = Date.now();
    }
    this.firemap.generateMap(this.seed);

    // Generate goal
    this.goal_type = GOALS_IDS[this.firemap.randomInt(0, GOALS_IDS.length - 1)];
    let goal_min = GOALS_MINS[this.goal_type];
    let goal_max = GOALS_MAXS[this.goal_type];
    if (this.goal_type == DESTRUCTION) {
      const flamable_cells = this.firemap.getFlamableCells();
      goal_min = Math.floor(0.25 * flamable_cells);
      goal_max = Math.floor(0.75 * flamable_cells);
    } else if (this.goal_type == ENERGY) {
      const fuel_available = this.firemap.getFuelAvailable();
      goal_min = Math.floor(0.25 * fuel_available);
      goal_max = Math.floor(0.75 * fuel_available);
    } else if (this.goal_type == HEAT) {
      const fuel_available = this.firemap.getFuelAvailable();
      goal_min = Math.floor(0.10 * fuel_available);
      goal_max = Math.floor(0.25 * fuel_available);
    }
    this.goal_value = this.firemap.randomInt(goal_min, goal_max);
    this.status.setGoal("Get " + GOALS_WORDS[this.goal_type] + " to " + this.goal_value);

    // Give some hints
    this.status.log("Game over when no fire remains");
    this.status.log("Time starts after first use of burn tool");

    // Describe ambient wind
    let ambient_wind_description = "none";
    let ambient_wind_descriptions = [];
    if (this.firemap.ambient_wind_y < 0) {
      ambient_wind_descriptions.push(-this.firemap.ambient_wind_y + " north");
    }
    if (this.firemap.ambient_wind_y > 0) {
      ambient_wind_descriptions.push(this.firemap.ambient_wind_y + " south");
    }
    if (this.firemap.ambient_wind_x > 0) {
      ambient_wind_descriptions.push(this.firemap.ambient_wind_x + " east");
    }
    if (this.firemap.ambient_wind_x < 0) {
      ambient_wind_descriptions.push(-this.firemap.ambient_wind_x + " west");
    }
    if (ambient_wind_descriptions.length > 0) {
      ambient_wind_description = ambient_wind_descriptions.join(", ");
    }
    this.status.setAmbientWind(ambient_wind_description);

    // Start paused
    this.energy = 0;
    this.max_heat = 0;
    this.tools.setToolCount(BURN, BURNS);
    this.started = false;
    this.menu.setStarted(this.started);
    this.won = false;
    this.menu.setWon(this.won);
    while (this.firemap.winds.length > 0) {
      this.firemap.shiftWind();
    }

    let self = this;
    this.time = 0;
    if (this.timer != null) {
      window.clearInterval(this.timer);
    }
    this.timer = window.setInterval(() => {self.tick();},
      (1000.0 / this.tick_rate) / Math.sqrt(this.streak + 1));
    this.render();
  }

  stop() {
    if (this.timer != null)
    {
      window.clearInterval(this.timer);
    }
  }

  tick() {
    const total_fuel_burned = this.firemap.getTotalFuelBurned();
    this.firemap.tickCells();

    const destruction = this.firemap.getTotalFireTransitions();
    this.status.setDestruction(destruction);

    const energy_earned = this.firemap.getTotalFuelBurned() - total_fuel_burned;
    this.energy += energy_earned;
    this.status.setEnergy(this.energy);

    const heat = this.firemap.getCurrentTotalHeat();
    this.max_heat = Math.max(this.max_heat, heat);
    this.status.setHeat(heat, this.max_heat);

    if (this.started) {
      this.time += 1;
    }
    this.status.setTime(this.time);

    if (!this.won) {
      if (this.goal_type == DESTRUCTION) {
        if (destruction >= this.goal_value) {
          this.status.log("Mission accomplished");
          this.won = true;
        }
      } else if (this.goal_type == ENERGY) {
        if (this.energy >= this.goal_value) {
          this.status.log("Mission accomplished");
          this.won = true;
        }
      } else if (this.goal_type == HEAT) {
        if (this.max_heat >= this.goal_value) {
          this.status.log("Mission accomplished");
          this.won = true;
        }
      } else if (this.goal_type == TIME) {
        if (this.time >= this.goal_value) {
          this.status.log("Mission accomplished");
          this.won = true;
        }
      }
      this.menu.setWon(this.won);
      if (this.won) {
        this.streak += 1;
        this.status.setStreak(this.streak);
      }
    }

    if (this.started && (this.firemap.getCurrentTotalFires() <= 0)) {
      this.status.log("The fire has died");
      this.started = false;
      this.menu.setStarted(this.started);
    }

    this.render();
  }

  useWind(cell_from, cell_to) {
    const wind_x = Math.max(-WIND_MAX_X, Math.min(cell_to.cell_x - cell_from.cell_x, WIND_MAX_X));
    const wind_y = Math.max(-WIND_MAX_Y, Math.min(cell_to.cell_y - cell_from.cell_y, WIND_MAX_Y));
    this.firemap.pushWind(cell_from.cell_x, cell_from.cell_y, WIND_RADIUS,
                          wind_x, wind_y);
    while (this.firemap.winds.length > MAX_WINDS) {
      this.firemap.shiftWind();
    }
  }
}

const WIDTH = 512;
const HEIGHT = 512;
const GRID_SIZE = 32;
const TICK_RATE = 1.0;

let canvas = document.getElementById("fireman-canvas");
let fireman = new Fireman(canvas, WIDTH, HEIGHT, GRID_SIZE, TICK_RATE);
fireman.start();
