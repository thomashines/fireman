class Status {
  constructor(context, input_handler, pos_x, pos_y, width_real, height_real) {
    this.context = context;
    this.input_handler = input_handler;
    this.pos_x = pos_x;
    this.pos_y = pos_y;
    this.width_real = width_real;
    this.height_real = height_real;

    this.font = "12px serif";
    this.y_offset = 16;
    this.column_start_x = [4, 100, 270];
    this.time_label = new Label(
      this.context, this.pos_x + this.column_start_x[0],
      this.pos_y + this.y_offset, this.font);
    this.energy_label = new Label(
      this.context, this.pos_x + this.column_start_x[0],
      this.pos_y + this.height_real / 4 + this.y_offset, this.font);
    this.ambient_wind_label = new Label(
      this.context, this.pos_x + this.column_start_x[0],
      this.pos_y + 2 * this.height_real / 4 + this.y_offset, this.font);
    this.detail_label = new Label(
      this.context, this.pos_x + this.column_start_x[0],
      this.pos_y + 3 * this.height_real / 4 + this.y_offset, this.font);
    this.destruction_label = new Label(
      this.context, this.pos_x + this.column_start_x[1],
      this.pos_y + this.y_offset, this.font);
    this.heat_label = new Label(
      this.context, this.pos_x + this.column_start_x[1],
      this.pos_y + this.height_real / 4 + this.y_offset, this.font);
    this.goal_label = new Label(
      this.context, this.pos_x + this.column_start_x[2],
      this.pos_y + this.y_offset, this.font);
    this.streak_label = new Label(
        this.context, this.pos_x + this.column_start_x[2],
        this.pos_y + this.height_real / 4 + this.y_offset, this.font),
    this.log_labels = [
      new Label(
        this.context, this.pos_x + this.column_start_x[2],
        this.pos_y + 2 * this.height_real / 4 + this.y_offset, this.font),
      new Label(
        this.context, this.pos_x + this.column_start_x[2],
        this.pos_y + 3 * this.height_real / 4 + this.y_offset, this.font),
    ];
    this.log_messages = [];

    this.setAmbientWind("none");
    this.setDestruction(0);
    this.setDetail("");
    this.setEnergy(0);
    this.setGoal("none");
    this.setHeat(0, 0);
    this.setStreak(0);
    this.setTime(0);
  }

  getActiveTool() {
    return this.active_tool;
  }

  log(message) {
    this.log_messages.unshift(message);
    while (this.log_messages.lenght > this.log_labels.length) {
      this.log_messages.pop();
    }
    for (let log_index = 0; log_index < this.log_labels.length; ++log_index) {
      if (log_index >= this.log_messages.length) {
        this.log_labels[log_index].setText("");
      } else {
        this.log_labels[log_index].setText(this.log_messages[log_index]);
      }
    }
  }

  render() {
    this.ambient_wind_label.render();
    this.destruction_label.render();
    this.detail_label.render();
    this.energy_label.render();
    this.goal_label.render();
    this.heat_label.render();
    this.streak_label.render();
    this.time_label.render();
    this.log_labels.forEach((label) => {label.render();});
  }

  setAmbientWind(ambient_wind) {
    this.ambient_wind_label.setText("Wind: " + ambient_wind);
  }

  setDestruction(destruction) {
    this.destruction_label.setText("Destruction: " + destruction);
  }

  setDetail(detail) {
    this.detail_label.setText(detail);
  }

  setEnergy(energy) {
    this.energy_label.setText("Energy: " + energy);
  }

  setGoal(goal) {
    this.goal_label.setText("Goal: " + goal);
  }

  setHeat(heat, max_heat) {
    if (heat == max_heat) {
      this.heat_label.setText("Heat: " + heat);
    } else {
      this.heat_label.setText("Heat: " + heat + " (max " + max_heat + ")");
    }
  }

  setStreak(streak) {
    this.streak_label.setText("Win streak: " + streak);
  }

  setTime(time) {
    this.time_label.setText("Time: " + time);
  }
}
