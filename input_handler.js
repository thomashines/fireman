class InputHandler {
  constructor(canvas, context = null) {
    this.canvas = canvas;
    this.context = context;

    this.mouse_events = [
      "click",
      "mousedown",
      "mousemove",
      "mouseup",
    ];
    this.mouse_handlers = {};

    let self = this;
    this.mouse_events.forEach((mouse_event) => {
      self.mouse_handlers[mouse_event] = [];
      self.canvas.addEventListener(mouse_event, (details) => {
        self.onMouseEvent(mouse_event, details);
      });
    });
  }

  onMouseEvent(mouse_event, details) {
    // Check something is listening
    if (this.mouse_handlers[mouse_event].length == 0) {
      return;
    }

    // Get the cursor position in the canvas
    const canvas_rect = this.canvas.getBoundingClientRect();
    const cursor_x = details.x - canvas_rect.x;
    const cursor_y = details.y - canvas_rect.y;

    // Draw the event for debugging purposes
    if (this.context != null) {
      this.context.strokeStyle = "#FFF";
      this.context.beginPath();
      this.context.arc(cursor_x, cursor_y, 8, 0, 2 * Math.PI);
      this.context.stroke();
    }

    // Call the handler callbacks
    this.mouse_handlers[mouse_event].forEach((handler) => {
      if ((handler[0] <= cursor_x) && (cursor_x <= handler[2]) &&
          (handler[1] <= cursor_y) && (cursor_y <= handler[3])) {
        handler[4](cursor_x, cursor_y, mouse_event, details);
      }
    });
  }

  registerMouseHandler(mouse_event, region_x1, region_y1, region_x2, region_y2, callback) {
    if (!this.mouse_handlers.hasOwnProperty(mouse_event))
    {
      console.log("event is not supported");
      return;
    }
    this.mouse_handlers[mouse_event].push([region_x1, region_y1, region_x2, region_y2, callback]);
  }
}
